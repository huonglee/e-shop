import React from 'react';
import ReactDOM from 'react-dom/client';
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from 'react-router-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import Home from './components/Home';
import Blog from './components/Blog/Blog';
import BlogDetail from './components/Blog/BlogDetail';
import Cart from './components/Cart/Cart';
import Index from './components/Member/Index';
import Account from './components/Account/Account';
import MyProduct from './components/Account/MyProduct';
import AddProduct from './components/Account/AddProduct';
import EditProduct from './components/Account/EditProduct';
import ProductDetail from './components/Product/ProductDetail';
import ProductWishlist from './components/Product/ProductWishlist';
import Checkout from './components/Checkout';
import Contact from './components/Contact';
import Error404 from './components/Errors/Error404';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Router>
      <App>
        <Routes>
          <Route index path='/' element={<Home />} />
          <Route path='/blog/list' element={<Blog />} />
          <Route path='/blog/detail/:id' element={<BlogDetail />} />
          <Route path='/login' element={<Index />} />
          <Route path='/product/cart' element={<Cart />} />
          <Route path='/user/update/:id' element={<Account />} />
          <Route path='/user/my-product' element={<MyProduct />} />
          <Route path='/user/product/add' element={<AddProduct />} />
          <Route path='/user/product/update/:id' element={<EditProduct />} />
          <Route path='/product/detail/:id' element={<ProductDetail />} />
          <Route path='/product/wishlist' element={<ProductWishlist />} />
          <Route path='/product/checkout' element={<Checkout />} />
          <Route path='/contact' element={<Contact />} />
          <Route path='/404' element={<Error404 />} />
        </Routes>

      </App>
    </Router>

  </React.StrictMode>
);


reportWebVitals();
