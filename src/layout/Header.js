import { Link } from "react-router-dom";

import { useContext, useState } from "react";

import { UserContext } from "../UserContext";


function Header(props) {

  function renderLogin() {
    if (localStorage.getItem("userData")) {
      let userData = JSON.parse(localStorage.getItem("userData"))
      return (
        <>
          <li><Link to={'/user/update/' + userData.Auth["id"]}><i className="fa fa-user"></i> Account</Link></li>
          <li><Link to='/login' onClick={handleLogout}><i className="fa fa-lock" /> Logout</Link></li>
        </>

      )
    } else {
      return <li><Link to='/login'><i className="fa fa-lock" /> Login</Link></li>
    }
  }

  function handleLogout() {
    localStorage.clear();
    renderLogin();
  }

  // use Context:
  const value = useContext(UserContext);

  function showCart() {
    let CartQty
    if (localStorage.getItem("CartQty")) {
      CartQty = JSON.parse(localStorage.getItem("CartQty"));
    }
    return CartQty;
  }

  function showWishlist() {
    let WishlistQty
    if (localStorage.getItem("WishlistQty")) {
      WishlistQty = JSON.parse(localStorage.getItem("WishlistQty"));
    }
    return WishlistQty;
  }

  return (
    <header id="header">{/*header*/}
      <div className="header_top">{/*header_top*/}
        <div className="container">
          <div className="row">
            <div className="col-sm-6">
              <div className="contactinfo">
                <ul className="nav nav-pills">
                  <li><Link><i className="fa fa-phone" /> +2 95 01 88 821</Link></li>
                  <li><Link><i className="fa fa-envelope" /> info@domain.com</Link></li>
                </ul>
              </div>
            </div>
            <div className="col-sm-6">
              <div className="social-icons pull-right">
                <ul className="nav navbar-nav">
                  <li><Link><i className="fa fa-facebook" /></Link></li>
                  <li><Link><i className="fa fa-twitter" /></Link></li>
                  <li><Link><i className="fa fa-linkedin" /></Link></li>
                  <li><Link><i className="fa fa-dribbble" /></Link></li>
                  <li><Link><i className="fa fa-google-plus" /></Link></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>{/*/header_top*/}
      <div className="header-middle">{/*header-middle*/}
        <div className="container">
          <div className="row">
            <div className="col-md-4 clearfix">
              <div className="logo pull-left">
                <Link to='/'><img src="../../../images/home/logo.png" alt="" /></Link>
              </div>
              <div className="btn-group pull-right clearfix">
                <div className="btn-group">
                  <button type="button" className="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                    USA
                    <span className="caret" />
                  </button>
                  <ul className="dropdown-menu">
                    <li><Link>Canada</Link></li>
                    <li><Link>UK</Link></li>
                  </ul>
                </div>
                <div className="btn-group">
                  <button type="button" className="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                    DOLLAR
                    <span className="caret" />
                  </button>
                  <ul className="dropdown-menu">
                    <li><Link>Canadian Dollar</Link></li>
                    <li><Link>Pound</Link></li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-md-8 clearfix">
              <div className="shop-menu clearfix pull-right">
                <ul className="nav navbar-nav">
                  <li><Link to='/product/wishlist'><i className="fa fa-star" /><sup>{value.wishlist || showWishlist()}</sup> Wishlist</Link></li>
                  <li><Link to='/product/checkout'><i className="fa fa-crosshairs" /> Checkout</Link></li>
                  <li><Link to='/product/cart'><i className="fa fa-shopping-cart" /><sup>{value.qty || showCart()}</sup> Cart</Link></li>
                  {renderLogin()}
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>{/*/header-middle*/}
      <div className="header-bottom">{/*header-bottom*/}
        <div className="container">
          <div className="row">
            <div className="col-sm-9">
              <div className="navbar-header">
                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span className="sr-only">Toggle navigation</span>
                  <span className="icon-bar" />
                  <span className="icon-bar" />
                  <span className="icon-bar" />
                </button>
              </div>
              <div className="mainmenu pull-left">
                <ul className="nav navbar-nav collapse navbar-collapse">
                  <li><Link to='/'>Home</Link></li>
                  <li className="dropdown"><Link>Shop<i className="fa fa-angle-down" /></Link>
                    <ul role="menu" className="sub-menu">
                      <li><Link>Products</Link></li>
                      <li><Link>Product Details</Link></li>
                      <li><Link>Checkout</Link></li>
                      <li><Link>Cart</Link></li>
                      <li><Link>Login</Link></li>
                    </ul>
                  </li>
                  <li className="dropdown"><Link>Blog<i className="fa fa-angle-down" /></Link>
                    <ul role="menu" className="sub-menu">
                      <li><Link to='/blog/list'>Blog List</Link></li>
                      <li><Link>Blog Single</Link></li>
                    </ul>
                  </li>
                  <li><Link to='/404'>404</Link></li>
                  <li><Link to='/contact'>Contact</Link></li>
                </ul>
              </div>
            </div>
            <div className="col-sm-3">
              <div className="search_box pull-right">
                <input type="text" placeholder="Search" />
              </div>
            </div>
          </div>
        </div>
      </div>{/*/header-bottom*/}
    </header>
  );
}
export default Header;