import { useState } from "react";
import './App.css';
import Header from './layout/Header';
import Footer from './layout/Footer';
import MenuLeft from './layout/MenuLeft';
import { useLocation } from "react-router-dom";
import MenuAcc from "./layout/MenuAcc";
import { UserContext } from "./UserContext"


function App(props) {
  let param1 = useLocation();
  const [qty, setQty] = useState("");
  const [wishlist, setWishlist] = useState("");


  function loginContext(data) {
    setQty(data)
  }

  function wishlistContext(data) {
    setWishlist(data)
  }

  return (
    <>
      <UserContext.Provider value={{
        loginContext: loginContext,
        wishlistContext: wishlistContext,
        qty: qty,
        wishlist: wishlist


      }}>
        <Header />
        <section>
          <div className="container">
            <div className="row">
              {param1["pathname"].includes("user") ? <MenuAcc /> : <MenuLeft />}
              {props.children}
            </div>
          </div>
        </section>
        <Footer />
      </UserContext.Provider>
    </>
  );
}

export default App;
