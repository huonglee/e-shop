import axios from "axios";
import { useEffect, useState, useContext } from "react";
import FormErrors from "../Errors/FormErrors";
import { UserContext } from "../../UserContext";

function Cart() {
  const value = useContext(UserContext);
  const [errors, setErrors] = useState("");
  const [listProduct, setListProduct] = useState("");
  // get auth.id
  let auth = (JSON.parse(localStorage.getItem("userData"))).Auth;
  let listCartProduct;
  
  // get data in localstorage:
  if (localStorage.getItem("listCartProduct")) {
    listCartProduct = JSON.parse(localStorage.getItem("listCartProduct"));
  }
  const data = listCartProduct;

  // gửi qua api:
  useEffect(() => {
    axios.post("http://localhost:8080/laravel8/public/api/product/cart", data)
      .then((res) => {
        console.log(res);
        if (res.data.errors) {
          setErrors(res.data.errors);
        } else {
          setListProduct(res.data.data);
        }
      })
      .catch(
        function (error) {
          console.log(error);
        })
  }, []);
  console.log("listProduct", listProduct);

  // Increase Quantity
  function AddIems(e) {
    let idPro = e.target.id;
    console.log("idPro", idPro);
    // copy ra 1 cai moi 
    let newData = [...listProduct]
    newData.map((item, index) => {
      if (idPro == item["id"]) {
        item["qty"] = item["qty"] + 1;
      }
    })
    setListProduct(newData);
    console.log("newListProduct", newData);

    // tang trong localstorage:
    Object.keys(listCartProduct).map((key, index) => {
      if (idPro == key) {
        listCartProduct[key] = listCartProduct[key] + 1;
      }
    })
    // lưu vào localstorage:
    localStorage.setItem("listCartProduct", JSON.stringify(listCartProduct));
    value.loginContext(CalCartQty())
  }

  // Decrease Quantity
  function DecreaseIems(e) {
    let idPro = e.target.id;
    console.log("", idPro);
    let newData = [...listProduct]
    newData.map((item, index) => {
      if (idPro == item["id"]) {
        if (item["qty"] > 1) {
          item["qty"] = item["qty"] - 1;
        }
      }
    })
    setListProduct(newData);
    console.log("newListProduct", newData);

    // giam trong localstorage:
    Object.keys(listCartProduct).map((key, index) => {
      if (idPro == key) {
        if (listCartProduct[key] > 1)
          listCartProduct[key] = listCartProduct[key] - 1;
      }
    })
    // lưu vào localstorage:
    localStorage.setItem("listCartProduct", JSON.stringify(listCartProduct));
    value.loginContext(CalCartQty())
  }

  // handle Delete
  function handleDelete(e) {
    let idPro = e.target.id;
    console.log("id", idPro);
    let newData = [...listProduct]
    newData.map((item, index) => {
      if (idPro == item["id"]) {
        newData = newData.filter(val => val != item);
        console.log("xx", newData)
      }
    })
    setListProduct(newData);
    console.log("newListProduct", newData);

    // xóa trong localstorage:
    Object.keys(listCartProduct).map((key, index) => {
      if (idPro == key) {
        delete listCartProduct[key];
      }
    })
    // lưu vào localstorage:
    localStorage.setItem("listCartProduct", JSON.stringify(listCartProduct));
    // tinh lai CartQty:
    value.loginContext(CalCartQty())
  }
  // hàm tinh tong sl product trong Cart:
  function CalCartQty() {
    let count = 0
    if (localStorage.getItem("listCartProduct")) {
      let listCartProduct = JSON.parse(localStorage.getItem("listCartProduct"))
      if (Object.keys(listCartProduct).length > 0) {
        Object.keys(listCartProduct).map((key, index) => {
          count = count + parseInt(listCartProduct[key]);
        })
      }
    }
    // lưu vào trong localstorage:
    localStorage.setItem("CartQty", JSON.stringify(count));
    return count
  }


  function showCartProducts() {
    if (listProduct.length > 0) {
      return listProduct.map((value, index) => {
        return (
          <tr key={index}>
            <td className="cart_product">
              <a><img src={"http://localhost:8080/laravel8/public/upload/product/" + auth.id + "/" + JSON.parse(value["image"])[0]} alt="" /></a>
            </td>
            <td className="cart_description">
              <h4><a>{value.name}</a></h4>
              <p>Web ID: {value.id}</p>
            </td>
            <td className="cart_price">
              <p>${value.price}</p>
            </td>
            <td className="cart_quantity">
              <div className="cart_quantity_button">
                <a className="cart_quantity_up" id={value.id} onClick={AddIems}> + </a>
                <input className="cart_quantity_input" type="text" name="quantity" value={value.qty} readOnly autoComplete="off" size={2} />
                <a className="cart_quantity_down" id={value.id} onClick={DecreaseIems}> - </a>
              </div>
            </td>
            <td className="cart_total">
              <p className="cart_total_price">${parseInt(value.price) * parseInt(value.qty)}</p>
            </td>
            <td className="cart_delete">
              <a className="cart_quantity_delete" id={value.id} onClick={handleDelete}><i className="fa fa-times" /></a>
            </td>
          </tr>
        )
      })
    }
  }

  function calCartTotal() {
    let totalCart = 0
    if (listProduct.length > 0) {
      listProduct.map((value, index) => {
        totalCart = parseInt(totalCart) + value.qty * parseInt(value.price);
      })
    }
    return totalCart;
  }

  return (
    <div className="col-sm-9 padding-right">
      <section id="cart_items">
        <div className="container">
          <FormErrors errors={errors} />
          <div className="table-responsive cart_info">
            <table className="table table-condensed">
              <thead>
                <tr className="cart_menu">
                  <td className="image">Item</td>
                  <td className="description" />
                  <td className="price">Price</td>
                  <td className="quantity">Quantity</td>
                  <td className="total">Total</td>
                  <td />
                </tr>
              </thead>
              <tbody>
                {showCartProducts()}
              </tbody>
            </table>
          </div>
        </div>
      </section> {/*/#cart_items*/}
      <section id="do_action">
        <div className="container">
          <div className="heading">
            <h3>What would you like to do next?</h3>
            <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
          </div>
          <div className="row">
            <div className="col-sm-6">
              <div className="chose_area">
                <ul className="user_option">
                  <li>
                    <input type="checkbox" />
                    <label>Use Coupon Code</label>
                  </li>
                  <li>
                    <input type="checkbox" />
                    <label>Use Gift Voucher</label>
                  </li>
                  <li>
                    <input type="checkbox" />
                    <label>Estimate Shipping &amp; Taxes</label>
                  </li>
                </ul>
                <ul className="user_info">
                  <li className="single_field">
                    <label>Country:</label>
                    <select>
                      <option>United States</option>
                      <option>Bangladesh</option>
                      <option>UK</option>
                      <option>India</option>
                      <option>Pakistan</option>
                      <option>Ucrane</option>
                      <option>Canada</option>
                      <option>Dubai</option>
                    </select>
                  </li>
                  <li className="single_field">
                    <label>Region / State:</label>
                    <select>
                      <option>Select</option>
                      <option>Dhaka</option>
                      <option>London</option>
                      <option>Dillih</option>
                      <option>Lahore</option>
                      <option>Alaska</option>
                      <option>Canada</option>
                      <option>Dubai</option>
                    </select>
                  </li>
                  <li className="single_field zip-field">
                    <label>Zip Code:</label>
                    <input type="text" />
                  </li>
                </ul>
                <a className="btn btn-default update">Get Quotes</a>
                <a className="btn btn-default check_out">Continue</a>
              </div>
            </div>
            <div className="col-sm-6">
              <div className="total_area">
                <ul>
                  <li>Cart Sub Total <span></span></li>
                  <li>Eco Tax <span>$2</span></li>
                  <li>Shipping Cost <span>Free</span></li>
                  <li>Total <span>{calCartTotal()}</span></li>
                </ul>
                <a className="btn btn-default update">Update</a>
                <a className="btn btn-default check_out">Check Out</a>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
export default Cart;