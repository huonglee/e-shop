import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import FormErrors from "../Errors/FormErrors";

function Login() {
  const navigate = useNavigate();
  const [inputs, setInputs] = useState({
    email: "",
    password: ""
  })
  const [errors, setErrors] = useState({});

  function handleInput(e) {
    const nameInput = e.target.name;
    const valueInput = e.target.value;
    setInputs(state => ({ ...state, [nameInput]: valueInput }))
  }

  function CheckEmail(inputText) {
    var regex = /^[A-Za-z][\w$.]+@[\w]+\.\w+$/;
    return regex.test(inputText);
  }

  function handleSubmit(e) {
    e.preventDefault();
    let errorsSubmit = {};
    let flag = true;
    // lay info tu localstorage:
    let info = localStorage.getItem("user_info")
    if (info) {
      info = JSON.parse(info);
    }
    // check da nhap email?
    if (inputs.email === "") {
      errorsSubmit.email = "Please enter email";
      flag = false;
    } else {
      // check email dung dịnh dang?
      if (!CheckEmail(inputs.email)) {
        errorsSubmit.email = "Invalid email";
        flag = false;
      } else {
        // check email nhap vao da dung vs email ben register chua?
        if (inputs.email !== info.email) {
          errorsSubmit.email = "Incorrect email";
          flag = false;
        }
      }
    }
    // check da nhap password?
    if (inputs.password === "") {
      errorsSubmit.password = "Please enter password";
      flag = false;
    } else {
      // check pass nhap vao da dung vs pass ben register chua?
      if (inputs.password !== info.password) {
        errorsSubmit.password = "Incorrect password";
        flag = false;
      }
    }

    if (flag) {
      const data = {
        email: inputs.email,
        password: inputs.password,
        level: 0
      }

      axios.post("http://localhost:8080/laravel8/public/api/login", data)
        .then((res) => {
          console.log(res);
          if (res.data.errors) {
            setErrors(res.data.errors);
          } else {
            // luu auth & token vao localstorage
            localStorage.setItem("userData", JSON.stringify(res.data));
            // chuyen den Home page.
            navigate("/");
          }
        })
        .catch(function (error) {
          console.log(error);
        })

    } else {
      setErrors(errorsSubmit);
    }
  }

  return (
    <div className="col-sm-4 col-sm-offset-1">
      <FormErrors errors={errors} />
      <div className="login-form">{/*login form*/}
        <h2>Login to your account</h2>
        <form onSubmit={handleSubmit}>
          <input type="email" placeholder="Email Address" name="email" onChange={handleInput} />
          <input type="password" placeholder="Password" name="password" onChange={handleInput} />
          <span>
            <input type="checkbox" className="checkbox" />
            Keep me signed in
          </span>
          <button type="submit" className="btn btn-default">Login</button>
        </form>
      </div>{/*/login form*/}
    </div>
  );
}
export default Login;