import axios from "axios";
import { useState } from "react";
import FormErrors from "../Errors/FormErrors";

function Register() {
    const [inputs, setInputs] = useState({
        name: "",
        email: "",
        password: "",
        phone: "",
        address: "",
        level: 0
    });
    const [getFile, setFile] = useState("");
    const [getAvatar, setAvatar] = useState("");
    const [errors, setErrors] = useState({});

    const handleUserInput = (e) => {
        const nameInput = e.target.name;
        const value = e.target.value;
        setInputs(state => ({ ...state, [nameInput]: value }));
    }

    // check mail address:
    function CheckEmail(inputText) {
        var regex = /^[A-Za-z][\w$.]+@[\w]+\.\w+$/;
        return regex.test(inputText);
    }

    // xử lí file:
    function handleUserInputFile(e) {
        const file = e.target.files;
        // send file to api server.
        let reader = new FileReader();
        reader.onload = (e) => {
            setAvatar(e.target.result); //cai nay gui qua api
            setFile(file[0]); //cai nay luu all thong tin file:name, size,...
        };
        reader.readAsDataURL(file[0]);
    }

    function checkFileType(fileName) {
        const fileExtension = ['png', 'jpg', 'jpeg', 'PNG', 'JPG'];
        let x = fileName.split(".");
        if (!fileExtension.includes(x[1])) {
            return false;
        } else {
            return true;
        }
    }

    function handleSubmit(e) {
        e.preventDefault();
        let errorsSubmit = {};
        let flag = true;
        // check da nhap name?
        if (inputs.name === "") {
            errorsSubmit.name = "Please enter name";
            flag = false;
        }
        // check da nhap email?
        if (inputs.email === "") {
            errorsSubmit.email = "Please enter email";
            flag = false;
        } else {
            // check định dạng email:
            if (!CheckEmail(inputs.email)) {
                errorsSubmit.email = "Invalid email";
                flag = false;
            }
        }
        // check da nhap password?
        if (inputs.password === "") {
            errorsSubmit.password = "Please enter password";
            flag = false;
        }
        // check da nhap phone?
        if (inputs.phone === "") {
            errorsSubmit.phone = "Please enter phone";
            flag = false;
        }
        // check da nhap address?
        if (inputs.address === "") {
            errorsSubmit.address = "Please enter address";
            flag = false;
        }
        // check da upload avatar?
        if (getFile === "") {
            errorsSubmit.file = "Please upload file";
            flag = false;
        } else {
            if (checkFileType(getFile['name']) === false) {
                errorsSubmit.file = "Invalid file type. Please upload image file!";
                flag = false;
            }
            else {
                if (getFile['size'] > (1024 * 1024)) {
                    errorsSubmit.file = "Maximum file size is 1mb!"
                    flag = false;
                }
            }
        }
        if (flag) {
            inputs["avatar"] = getAvatar;
            // lưu vào localstorage
            localStorage.setItem("user_info", JSON.stringify(inputs));
            // gửi qua api
            const data = {
                name: inputs.name,
                email: inputs.email,
                password: inputs.password,
                phone: inputs.phone,
                address: inputs.address,
                level: 0,
                avatar: getAvatar
            }
            axios.post("http://localhost:8080/laravel8/public/api/register", data)
                .then((res) => {
                    console.log(res);
                    if (res.data.errors) {
                        setErrors(res.data.errors);
                    } else {
                        alert("Register success!");
                    }

                }).catch(function (error) {
                    console.log(error);
                })
        } else {
            setErrors(errorsSubmit);
        }
    }

    return (
        <div className="col-sm-4">
            <div className="signup-form">{/*sign up form*/}
                <h2>New User Signup!</h2>
                <FormErrors errors={errors} />
                <form encType="multipart/form-data" onSubmit={handleSubmit}>
                    <input type="text" placeholder="Name" name="name" onChange={handleUserInput} />
                    <input type="email" placeholder="Email Address" name="email" onChange={handleUserInput} />
                    <input type="password" placeholder="Password" name="password" onChange={handleUserInput} />
                    <input type="number" placeholder="Phone" name="phone" onChange={handleUserInput} />
                    <input type="address" placeholder="Address" name="address" onChange={handleUserInput} />
                    <input type="file" placeholder="Avatar" onChange={handleUserInputFile} />
                    <input type="number" placeholder="Level" name="level" defaultValue={0} />
                    <button type="submit" className="btn btn-default">Signup</button>
                </form>
            </div>{/*/sign up form*/}
        </div>
    )
}
export default Register;