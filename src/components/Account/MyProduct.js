import axios from "axios";
import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";

function MyProduct() {
  const navigate = useNavigate();
  function handleAddNewProduct() {
    navigate("/user/product/add")
  }
  const [products, setProduct] = useState("");
  let accessToken = (JSON.parse(localStorage.getItem("userData"))).token;
  let auth = (JSON.parse(localStorage.getItem("userData"))).Auth;

  // config de gui token qua API
  let config = {
    headers: {
      'Authorization': 'Bearer ' + accessToken,
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json'
    }
  };

  useEffect(() => {
    axios.get("http://localhost:8080/laravel8/public/api/user/my-product", config)
      .then(
        response => {
          console.log("listproduct", response.data.data);
          setProduct(response.data.data);
        }
      ).catch(
        error => {
          console.log(error);
        }
      )
  }, []);


  function renderAllProduct() {
    if (Object.keys(products).length > 0) {
      return Object.keys(products).map((key, index) => {
        return (
          <>
            <tr key={index}>
              <td className="product_id">
                <p>{products[key]["id"]}</p>
              </td>
              <td className="product_name">
                <p>{products[key]["name"]}</p>
              </td>
              <td className="product_image">
                <img src={"http://localhost:8080/laravel8/public/upload/product/" + auth.id + "/" + JSON.parse(products[key]["image"])[0]} alt="" />
              </td>
              <td className="product_price">
                <p>${products[key]["price"]}</p>
              </td>
              <td className="product_edit_action">
                <Link className="product_edit" to={"/user/product/update/" + products[key]["id"]}><i className="fa fa-pencil-square-o"></i></Link>
              </td>
              <td className="product_delete_action">
                <a className="product_delete" onClick={() => { handleDeleteProduct(products[key]["id"]) }}><i className="fa fa-times"></i></a>
              </td>
            </tr>
          </>
        )
      })
    }
  }

  function handleDeleteProduct(idProduct) {
    axios.get("http://localhost:8080/laravel8/public/api/user/product/delete/" + idProduct, config)
      .then(
        response => {
          console.log("listproduct", response.data.data);
          setProduct(response.data.data);
          alert("Delete product success!")
        }
      ).catch(
        error => {
          console.log(error);
        }
      )
  }

  return (
    <div className="col-sm-9 padding-right">
      <section id="cart_items">
        <div className="container">
          <div className="table-responsive cart_info">
            <table className="table table-condensed">
              <thead>
                <tr className="cart_menu">
                  <td className="product_id">Id</td>
                  <td className="product_name">Name</td>
                  <td className="product_image">Image</td>
                  <td className="product_price">Price</td>
                  <td className="action">Action</td>
                  <td />
                </tr>
              </thead>
              <tbody>
                {renderAllProduct()}
              </tbody>
            </table>
          </div>
          <button type="submit" id="add_new_product" className="btn btn-default" onClick={handleAddNewProduct}>Add New</button>
        </div>
      </section>
    </div>
  )
}
export default MyProduct;