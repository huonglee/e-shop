import axios from "axios";
import { useState } from "react";
import { useParams } from "react-router-dom";
import FormErrors from "../Errors/FormErrors";

function Account() {
    let params = useParams();
    let userInfo;
    if (localStorage.getItem("user_info")) {
        userInfo = JSON.parse(localStorage.getItem("user_info"));
    }
    const [errors, setErrors] = useState("");
    const [getFile, setFile] = useState("");
    const [getAvatar, setAvatar] = useState(userInfo.avatar);
    const [getInputs, setInputs] = useState({
        name: userInfo.name,
        email: userInfo.email,
        password: userInfo.password,
        phone: userInfo.phone,
        address: userInfo.address,
        level: userInfo.level
    });

    const handleInput = (e) => {
        const inputValue = e.target.value;
        const inputName = e.target.name;
        setInputs(state => ({ ...state, [inputName]: inputValue }));
    };

    function handleFile(e) {
        const file = e.target.files;
        let reader = new FileReader();
        reader.onload = (e) => {
            setAvatar(e.target.result);
            setFile(file[0]);
        }
        reader.readAsDataURL(file[0]);
    }

    function checkFileType(fileName) {
        const fileExtension = ['png', 'jpg', 'jpeg', 'PNG', 'JPG'];
        let x = fileName.split(".");
        if (!fileExtension.includes(x[1])) {
            return false;
        } else {
            return true;
        }
    }

    function handleSubmit(e) {
        e.preventDefault();
        let flag = true;
        let errorsSubmit = {}
        // check da nhap name chua?
        if (getInputs.name === "") {
            errorsSubmit.name = "Please enter name"
            flag = false;
        }
        // check da nhap password?
        if (getInputs.password === "") {
            errorsSubmit.password = "Please enter password";
            flag = false;
        }
        // check da nhap phone?
        if (getInputs.phone === "") {
            errorsSubmit.phone = "Please enter phone";
            flag = false;
        }
        // check da nhap address?
        if (getInputs.address === "") {
            errorsSubmit.address = "Please enter address";
            flag = false;
        }
        // check avatar upload
        if (getFile !== "") {
            if (checkFileType(getFile['name']) === false) {
                errorsSubmit.file = "Invalid file type. Please upload image file!";
                flag = false;
            }
            else {
                if (getFile['size'] > (1024 * 1024)) {
                    errorsSubmit.file = "Maximum file size is 1mb!"
                    flag = false;
                }
            }
        }

        if (flag) {
            // luu info vao localStorage:
            localStorage.setItem("user_info", JSON.stringify(getInputs));
            let accessToken = (JSON.parse(localStorage.getItem("userData"))).token;
            // config de gui token qua API
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            };

            const formData = new FormData();
            formData.append("name", getInputs.name);
            formData.append("email", getInputs.email);
            formData.append("password", getInputs.password);
            formData.append("phone", getInputs.phone);
            formData.append("address", getInputs.address);
            formData.append("level", getInputs.level);
            formData.append("avatar", getAvatar);
            // gui info qua api:
            axios.post("http://localhost:8080/laravel8/public/api/user/update/" + params.id, formData, config)
                .then(
                    (response) => {
                        console.log(response);
                        if (response.data.errors) {
                            setErrors(response.data.errors);
                        } else {
                            alert("Update success!");
                        }
                    })
                .catch(
                    error => {
                        console.log(error);
                    }
                )

        } else {
            setErrors(errorsSubmit);
        }
    }

    return (
        <div className="col-sm-4">
            <div className="signup-form">
                <h2>User Update!</h2>
                <FormErrors errors={errors} />
                <form encType="multipart/form-data" onSubmit={handleSubmit}>
                    <input type="text" placeholder="Name" name="name" defaultValue={userInfo.name} onChange={handleInput} />
                    <input type="email" placeholder="Email Address" name="email" readOnly defaultValue={userInfo.email} />
                    <input type="password" placeholder="Password" name="password" defaultValue={userInfo.password} onChange={handleInput} />
                    <input type="number" placeholder="Phone" name="phone" defaultValue={userInfo.phone} onChange={handleInput} />
                    <input type="address" placeholder="Address" name="address" defaultValue={userInfo.address} onChange={handleInput} />
                    <input type="file" placeholder="Avatar" onChange={handleFile} />
                    <input type="number" placeholder="Level" name="level" readOnly defaultValue={userInfo.level} />
                    <button type="submit" className="btn btn-default">Update</button>
                </form>
            </div>
        </div>
    )
}
export default Account;