import axios from "axios";
import { useEffect, useState } from "react"
import { useParams } from "react-router-dom";
import FormErrors from "../Errors/FormErrors";

function EditProduct() {
  const [errors, setErrors] = useState("");
  const [listImage, setListImage] = useState([]);
  const [category, setCategory] = useState("");
  const [brand, setBrand] = useState("");
  const [avatarCheckbox, setAvatarCheckbox] = useState([]);
  // product information field:
  const [getName, setName] = useState("");
  const [price, setPrice] = useState("");
  const [categorys, setCategorys] = useState("");
  const [brands, setBrands] = useState("");
  const [status, setStatus] = useState("");
  const [sale, setSale] = useState("");
  const [detail, setDetail] = useState("");
  const [company, setCompany] = useState("");
  // file upload:
  const [files, setFiles] = useState("");

  let params = useParams();
  let accessToken = (JSON.parse(localStorage.getItem("userData"))).token;
  let auth = (JSON.parse(localStorage.getItem("userData"))).Auth;
  let config = {
    headers: {
      'Authorization': 'Bearer ' + accessToken,
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json'
    }
  };

  // get thong tin product show tren form:
  useEffect(() => {
    axios.get("http://localhost:8080/laravel8/public/api/user/product/" + params.id, config)
      .then(
        response => {
          console.log("Infoproduct", response.data.data);

          // get list image cua product:
          setListImage(response.data.data.image);

          // get product information:
          setName(response.data.data.name);
          setPrice(response.data.data.price);
          setCategorys(response.data.data.id_category);
          setBrands(response.data.data.id_brand);
          setStatus(response.data.data.status);
          if (response.data.data.sale === null) {
            setSale("");
          } else {
            setSale(response.data.data.sale);
          }
          setDetail(response.data.data.detail);
          setCompany(response.data.data.company_profile);
        }
      ).catch(
        error => {
          console.log(error);
        }
      )
  }, []);

  const inputs = {
    name: getName,
    price: price,
    category: categorys,
    brand: brands,
    status: status,
    sale: sale,
    detail: detail,
    company: company,
  };


  // get category and brand:
  useEffect(() => {
    axios.get("http://localhost:8080/laravel8/public/api/category-brand")
      .then(
        response => {
          console.log(response);
          setCategory(response.data.category);
          setBrand(response.data.brand);
        })
      .catch(
        error => {
          console.log(error);
        }
      )
  }, []);

  function renderCategory() {
    if (category.length > 0) {
      return category.map((value, key) => {
        return (<option key={key} value={value["id"]}>{value["category"]}</option>)
      })
    }
  }

  function renderBrand() {
    if (brand.length > 0) {
      return brand.map((value, key) => {
        return (<option key={key} value={value["id"]}>{value["brand"]}</option>)
      })
    }
  }

  function showImage() {
    if (listImage.length > 0) {
      return listImage.map((value, key) => {
        return (
          <li key={key}>
            <img src={"http://localhost:8080/laravel8/public/upload/product/" + auth.id + "/" + value} />
            <input type="checkbox" id={key} value={value} onClick={handleCheckbox} ></input>
          </li>)
      })
    }
  }

  function handleCheckbox(e) {
    if (e.target.checked === true) {
      setAvatarCheckbox([...avatarCheckbox, e.target.value]);
    }
    else {
      let freshArray = avatarCheckbox.filter(val => val !== e.target.value);
      setAvatarCheckbox([...freshArray]);
    }
  }

  function handleFile(e) {
    console.log("files:", e.target.files);
    let listfile = {}
    if (Object.keys(e.target.files).length > 0) {
      Object.keys(e.target.files).map((item, i) => {
        listfile[i] = e.target.files[item];
      })
    }
    setFiles(listfile);
  }

  function checkImageFile(listFile) {
    const fileExtension = ['png', 'jpg', 'jpeg', 'PNG', 'JPG'];
    let flag = true;
    for (let item in listFile) {
      let xx = listFile[item]["name"].split(".");
      if (fileExtension.includes(xx[1]) === false) {
        flag = false;
        break;
      } else {
        flag = true;
      }
    }
    return flag;
  }

  function checkImageFileSize(listFile) {
    let flag = true;
    for (let item in listFile) {
      if (listFile[item]["size"] > (1024 * 1014)) {
        flag = false;
        break;
      } else {
        flag = true;
      }
    }
    return flag;
  }

  function handleSubmit(e) {
    e.preventDefault();
    let errorsSubmit = {}
    let flag = true;

    // check name product field:
    if (inputs.name === "") {
      errorsSubmit.name = "Please enter product name!";
      flag = false;
    }
    // check price product field:
    if (inputs.price === "") {
      errorsSubmit.price = "Please enter product price!";
      flag = false;
    }
    // check category product field:
    if (inputs.category === "") {
      errorsSubmit.category = "Please choose product category!";
      flag = false;
    }
    // check brand product field:
    if (inputs.brand === "") {
      errorsSubmit.brand = "Please choose product brand!";
      flag = false;
    }
    // check status product field:
    if (inputs.status === "") {
      errorsSubmit.status = "Please choose product status!";
      flag = false;
    }
    // check detail product field:
    if (inputs.detail === "") {
      errorsSubmit.detail = "Please enter product detail!";
      flag = false;
    }
    // check company product field:
    if (inputs.company === "") {
      errorsSubmit.company = "Please enter product company!";
      flag = false;
    }
    // check files upload:
    if (files === "") {
      errorsSubmit.file = "Please upload image file!";
      flag = false;
    } else {
      if ((Object.keys(files).length + listImage.length - avatarCheckbox.length) > 3) {
        errorsSubmit.file = "The number of uploaded files is larger than the allowed number!"
        flag = false;
      } else {
        if (checkImageFile(files) === false) {
          errorsSubmit.file = "Invalid file type. Please upload image file!";
          flag = false;
        } else {
          if (checkImageFileSize(files) == false) {
            errorsSubmit.fileSize = "Maximum file size is 1mb!";
            flag = false;
          }
        }
      }
    }

    if (flag) {
      // gửi qua api:
      console.log(inputs);
      const formData = new FormData();
      formData.append("name", inputs.name);
      formData.append("price", inputs.price);
      formData.append("category", inputs.category);
      formData.append("brand", inputs.brand);
      formData.append("status", inputs.status);
      formData.append("sale", inputs.sale);
      formData.append("detail", inputs.detail);
      formData.append("company", inputs.company);

      Object.keys(files).map((item, i) => {
        formData.append("file[]", files[item]);
      });

      avatarCheckbox.map((value, index) => {
        formData.append("avatarCheckBox[]", value);
      });
      axios.post("http://localhost:8080/laravel8/public/api/user/product/update/" + params.id, formData, config)
        .then(
          response => {
            console.log(response);
            if (response.data.errors) {
              setErrors(response.data.errors);
            } else {
              alert("Edit product success!");
            }
          }
        )
        .catch(
          error => {
            console.log(error.response.data);
          }
        )
    } else {
      setErrors(errorsSubmit);
    }
  }
  return (
    <div className="col-sm-6">
      <div className="signup-form">
        <h2>Product Update!</h2>
        <FormErrors errors={errors} />
        <form encType="multipart/form-data" onSubmit={handleSubmit}>
          <input type="text" placeholder="Name" name="name" value={getName} onChange={(e) => { setName(e.target.value) }} />
          <input type="number" placeholder="Price" name="price" value={price} onChange={(e) => { setPrice(e.target.value) }} />
          <select name="category" value={categorys} onChange={(e) => { setCategorys(e.target.value) }}>
            {renderCategory()}
          </select>
          <select name="brand" value={brands} onChange={(e) => { setBrands(e.target.value) }}>
            {renderBrand()}
          </select>
          <select name="status" value={status} onChange={(e) => { setStatus(e.target.value) }}>
            <option value="">Please choose status</option>
            <option value="0">New</option>
            <option value="1">Sale</option>
          </select>
          <input type="number" placeholder="Sale" name="sale" value={sale} onChange={(e) => { setSale(e.target.value) }} />
          <input type="file" name="image" multiple onChange={handleFile} />
          <ul className="edit_product_image">
            {showImage()}
          </ul>
          <textarea type="text" placeholder="Detail" name="detail" value={detail} onChange={(e) => { setDetail(e.target.value) }}></textarea>
          <input type="text" placeholder="Company" name="company" value={company} onChange={(e) => { setCompany(e.target.value) }} />
          <button type="submit" className="btn btn-default">Update</button>
        </form>
      </div>
    </div>
  )
}
export default EditProduct