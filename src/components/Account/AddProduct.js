import axios from "axios";
import { useEffect, useState } from "react";
import FormErrors from "../Errors/FormErrors";

function AddProduct() {
    const [errors, setErrors] = useState("");
    const [category, setCategory] = useState([]);
    const [brand, setBrand] = useState([]);
    const [getStatus, setStatus] = useState("");
    const [inputs, setInput] = useState({
        name: "",
        price: "",
        category: "",
        brand: "",
        status: "",
        sale: "",
        detail: "",
        company: "",
    });
    const [files, setFiles] = useState("");

    useEffect(() => {
        axios.get("http://localhost:8080/laravel8/public/api/category-brand")
            .then(
                response => {
                    console.log(response);
                    setCategory(response.data.category);
                    setBrand(response.data.brand);
                })
            .catch(
                error => {
                    console.log(error);
                }
            )
    }, [])

    function renderCategory() {
        if (category.length > 0) {
            return category.map((value, key) => {
                return (<option key={key} value={value["id"]}>{value["category"]}</option>)
            })
        }
    }

    function renderBrand() {
        if (brand.length > 0) {
            return brand.map((value, key) => {
                return (<option key={key} value={value["id"]}>{value["brand"]}</option>)
            })
        }
    }

    function handleStatus(e) {
        setStatus(e.target.value);
    }

    function renderSale() {
        if (getStatus === "1") {
            return <input type="number" placeholder="Sale" name="sale" onChange={handleInput} />
        }
    }

    function handleInput(e) {
        const nameInput = e.target.name;
        const valueInput = e.target.value;
        setInput(state => ({ ...state, [nameInput]: valueInput }));

    }

    function handleFile(e) {
        console.log("files:", e.target.files);
        let listfile = {}
        Object.keys(e.target.files).map((item, i) => {
            listfile[i] = e.target.files[item];
        })
        setFiles(listfile);
    }

    function checkImageFile(listFile) {
        const fileExtension = ['png', 'jpg', 'jpeg', 'PNG', 'JPG'];
        let flag = true;
        for (let item in listFile) {
            let xx = listFile[item]["name"].split(".");
            if (fileExtension.includes(xx[1]) === false) {
                flag = false;
                break;
            } else {
                flag = true;
            }
        }
        return flag;
    }

    function checkImageFileSize(listFile) {
        let flag = true;
        for (let item in listFile) {
            if (listFile[item]["size"] > (1024 * 1014)) {
                flag = false;
                break;
            } else {
                flag = true;
            }
        }
        return flag;
    }

    function handleCreateProduct(e) {
        e.preventDefault();
        let errorsSubmit = {};
        let flag = true;
        // check name field:
        if (inputs.name === "") {
            errorsSubmit.name = "Please enter name product!"
            flag = false;
        }
        // check price field:
        if (inputs.price === "") {
            errorsSubmit.price = "Please enter price product!"
            flag = false;
        }
        // check category field:
        if (inputs.category === "") {
            errorsSubmit.category = "Please choose categogy product!"
            flag = false;
        }
        // check brand field:
        if (inputs.brand === "") {
            errorsSubmit.brand = "Please choose brand product!"
            flag = false;
        }
        // check status field:
        if (inputs.status === "") {
            errorsSubmit.status = "Please choose status product!"
            flag = false;
        }
        // check detail field:
        if (inputs.detail === "") {
            errorsSubmit.detail = "Please enter detail product!"
            flag = false;
        }
        // check company field:
        if (inputs.company === "") {
            errorsSubmit.company = "Please enter company name!"
            flag = false;
        }
        // check upload files images:
        if (files === "") {
            errorsSubmit.image = "Please upload images!";
            flag = false;
        } else {
            if (Object.keys(files).length > 3) {
                errorsSubmit.file = "You can only upload a maximum of 3 image files!";
                flag = false;
            } else {
                if (checkImageFile(files) == false) {
                    errorsSubmit.file = "Invalid file type. Please upload image file!";
                    flag = false;
                } else {
                    if (checkImageFileSize(files) == false) {
                        errorsSubmit.fileSize = "Maximum file size is 1mb!";
                        flag = false;
                    }
                }
            }
        }

        if (flag) {
            // luu product info vao localStorage:
            inputs.image = files
            console.log(inputs);
            localStorage.setItem("productInfo", JSON.stringify(inputs));

            let accessToken = (JSON.parse(localStorage.getItem("userData"))).token;
            // config de gui token qua API
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            };

            const formData = new FormData();
            formData.append("name", inputs.name);
            formData.append("price", inputs.price);
            formData.append("category", inputs.category);
            formData.append("brand", inputs.brand);
            formData.append("status", inputs.status);
            formData.append("sale", inputs.sale);
            formData.append("detail", inputs.detail);
            formData.append("company", inputs.company);

            Object.keys(files).map((item, i) => {
                formData.append("file[]", files[item]);
            })

            // gui product info qua api:
            axios.post("http://localhost:8080/laravel8/public/api/user/product/add", formData, config)
                .then(
                    (response) => {
                        console.log(response);
                        if (response.data.errors) {
                            setErrors(response.data.errors);
                        } else {
                            alert("Create product success!");
                        }
                    })
                .catch(
                    error => {
                        console.log(error);
                    }
                )

        } else {
            setErrors(errorsSubmit);
        }
    }

    return (
        <div className="col-sm-4">
            <div className="signup-form">
                <h2>Create Product!</h2>
                <FormErrors errors={errors} />
                <form encType="multipart/form-data" onSubmit={handleCreateProduct}>
                    <input type="text" placeholder="Name" name="name" onChange={handleInput} />
                    <input type="number" placeholder="Price" name="price" onChange={handleInput} />
                    <select name="category" onChange={handleInput}>
                        <option value="">Please choose category</option>
                        {renderCategory()}
                    </select>
                    <select name="brand" onChange={handleInput}>
                        <option value="">Please choose brand</option>
                        {renderBrand()}
                    </select>
                    <input type="file" name="image" multiple onChange={handleFile} />
                    <select name="status" onClick={handleStatus} onChange={handleInput}>
                        <option value="">Please choose status</option>
                        <option value="0">New</option>
                        <option value="1">Sale</option>
                    </select>
                    {renderSale()}
                    <textarea type="text" placeholder="Detail" name="detail" onChange={handleInput}></textarea>
                    <input type="text" placeholder="Company" name="company" onChange={handleInput} />
                    <button type="submit" className="btn btn-default">Create</button>
                </form>
            </div>
        </div>
    )
}
export default AddProduct;