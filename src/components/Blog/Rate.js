import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import StarRatings from "react-star-ratings";
import FormErrors from "../Errors/FormErrors";

function Rate() {
  const [rating, setRating] = useState(0);
  const [errors, setErrors] = useState("");
  let params = useParams();

  function changeRating(newRating, name) {
    let flag = true;
    let errorsSubmit = {};
    // kt login roi moi dc danh gia
    if (!localStorage.getItem("userData")) {
      errorsSubmit.rate = "Please login!"
      flag = false;
    }
    if (flag) {
      // da login --> danh gia
      setRating(newRating);
      // - xu ly logic va api
      let accessToken = (JSON.parse(localStorage.getItem("userData"))).token;
      // config:
      let config = {
        headers: {
          'Authorization': 'Bearer ' + accessToken,
          'Content-Type': 'application/x-www-form-urlencoded',
          'Accept': 'application/json'
        }
      };

      let auth = JSON.parse(localStorage.getItem("userData")).Auth;
      const formData = new FormData();
      formData.append("blog_id", params.id);
      formData.append("user_id", auth.id);
      formData.append("rate", newRating);

      axios.post("http://localhost:8080/laravel8/public/api/blog/rate/id", formData, config)
        .then(response => {
          console.log(response)
          if (response.data.errors) {
            setErrors(response.data.errors);
          } else {
            alert("Rating success.");
          }

        })
    } else {
      setErrors(errorsSubmit);
    }
  }

  useEffect(() => {
    axios.get("http://localhost:8080/laravel8/public/api/blog/rate/" + params.id)
      .then(
        response => {
          let listRate = response.data.data;
          console.log("listRate:", listRate)
          let tong = 0;
          if (listRate.length > 0) {
            listRate.map((value, index) => {
              tong = tong + value.rate;
            })
            // reload trang => hien thi ra trung binh cong rate.
            console.log("avg rate:", tong / listRate.length);
            setRating(tong / listRate.length);
          }
        }
      )
      .catch(
        error => {
          console.log(error);
        })
  }, []);

  return (
    <div className="rating-area">
      <FormErrors errors={errors} />
      <ul className="ratings">
        <li className="rate-this">Rate this item:</li>
        <li>
          <StarRatings
            rating={rating}
            starRatedColor="orange"
            changeRating={changeRating}
            numberOfStars={6}
            name='rating'
          />
        </li>
        <li className="color">(6 votes)</li>
      </ul>
      <ul className="tag">
        <li>TAG:</li>
        <li><a className="color">Pink <span>/</span></a></li>
        <li><a className="color">T-Shirt <span>/</span></a></li>
        <li><a className="color">Girls</a></li>
      </ul>
    </div>
  )
}
export default Rate;