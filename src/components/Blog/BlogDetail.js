import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Comment from "./Comment";
import ListComment from "./ListComment";
import Rate from "./Rate";

function BlogDetail(props) {
  // lấy id trên url
  let params = useParams();

  const [data, setData] = useState('');
  const [listC, setList] = useState("");
  const [idCha, setIdCha] = useState(0);

  useEffect(() => {
    axios.get("http://localhost:8080/laravel8/public/api/blog/detail/" + params.id)
      .then(res => {
        setData(res.data.data);
        setList(res.data.data.comment);
      })
      .catch(error => console.log(error))
  }, [])

  function fetchData() {
    if (Object.keys(data).length > 0) {
      return (
        <div className="single-blog-post">
          <h3>{data['title']}</h3>
          <div className="post-meta">
            <ul>
              <li><i className="fa fa-user" /> Mac Doe</li>
              <li><i className="fa fa-clock-o" /> 1:33 pm</li>
              <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
            </ul>
          </div>
          <a>
            <img src={"http://localhost:8080/laravel8/public/upload/Blog/image/" + data['image']} alt="" />
          </a>
          <p>
            {data['content']};
          </p>
          <div className="pager-area">
            <ul className="pager pull-right">
              <li><a>Pre</a></li>
              <li><a>Next</a></li>
            </ul>
          </div>
        </div>
      )
    }
  }

  function getCmt(data) {
    let newlist = listC.concat(data);
    setList(newlist);
    console.log("newlist", newlist);
  }

  function getIdCha(x) {
    setIdCha(x);
    console.log("idcha:", idCha);
  }

  return (
    <div className="col-sm-9">
      <div className="blog-post-area">
        <h2 className="title text-center">Latest From our Blog</h2>
        {fetchData()}
      </div>{/*/blog-post-area*/}
      <Rate />
      <div className="socials-share">
        <a><img src="../../../images/blog/socials.png" alt="" /></a>
      </div>{/*/socials-share*/}
      <ListComment listcmt={listC} getIdCha={getIdCha} />
      <div className="replay-box">
        <div className="row">
          <div className="col-sm-12">
            <h2>Leave a replay</h2>
            <Comment getCmt={getCmt} idCha={idCha} />
          </div>
        </div>
      </div>
    </div>
  );
}
export default BlogDetail;