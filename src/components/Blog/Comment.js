import axios from "axios";
import { useState } from "react";
import { useParams } from "react-router-dom";
import FormErrors from "../Errors/FormErrors";

function Comment(props) {
    console.log("comment idcha:", props.idCha);
    const [comment, setComment] = useState("");
    const [errors, setErrors] = useState({});
    const handleInputComment = (e) => {
        setComment(e.target.value);
    }
    let params = useParams();

    function handleSubmitComment(e) {
        e.preventDefault();
        let errorsSubmit = {}
        let flag = true;
        if (localStorage.getItem("userData")) {
            if (comment === "") {
                errorsSubmit.comment = "Please enter comment.";
                flag = false;
            }
        } else {
            errorsSubmit.login = "Please login.";
            flag = false;
        }

        if (!flag) {
            setErrors(errorsSubmit);
        } else {
            // comment da dc nhap
            let accessToken = (JSON.parse(localStorage.getItem("userData"))).token;
            // config de gui token qua API
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            };

            let auth = JSON.parse(localStorage.getItem("userData")).Auth;
            const formData = new FormData();
            formData.append("id_blog", params.id);
            formData.append("id_user", auth.id);
            formData.append("id_comment", props.idCha);
            formData.append("comment", comment);
            formData.append("image_user", auth.avatar);
            formData.append("name_user", auth.name);

            axios.post("http://localhost:8080/laravel8/public/api/blog/comment/id", formData, config)
                .then(response => {
                    console.log(response);
                    if (response.data.errors) {
                        setErrors(response.data.errors);
                    } else {
                        // lay comment de truyen --> component BlogDetail
                        props.getCmt(response.data.data);
                        alert("Comment success");
                    }
                })
        }
    }

    return (
        <div className="text-area">
            <div className="blank-arrow">
                <label>Your Name</label>
            </div>
            <span>*</span>
            <FormErrors errors={errors} />
            <textarea id="cmt" name="message" rows={11} defaultValue={""} onChange={handleInputComment} />
            <a className="btn btn-primary" onClick={handleSubmitComment}>post comment</a>
        </div>
    )
}
export default Comment;