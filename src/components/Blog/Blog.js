import { useState } from "react";
import axios from "axios";
import { useEffect } from "react";
import { Link } from "react-router-dom";

function Blog() {
  const [getData, setData] = useState([]);
  useEffect(() => {
    axios.get("http://localhost:8080/laravel8/public/api/blog").then
      (res => { setData(res.data.blog) })
      .catch(error => console.log(error))
  }, [])

  function fetchData() {
    if (Object.keys(getData).length > 0) {
      return getData['data'].map((value, key) => {
        return (
          <div className="single-blog-post" key={key}>
            <h3>{value['title']}</h3>
            <div className="post-meta">
              <ul>
                <li><i className="fa fa-user" /> Mac Doe</li>
                <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
              </ul>
              <span>
                <i className="fa fa-star" />
                <i className="fa fa-star" />
                <i className="fa fa-star" />
                <i className="fa fa-star" />
                <i className="fa fa-star-half-o" />
              </span>
            </div>
            <a>
              <img src={"http://localhost:8080/laravel8/public/upload/Blog/image/" + value['image']} alt="" />
            </a>
            <p>{value['description']}</p>
            <Link className="btn btn-primary" to={"/blog/detail/" + value['id']}>Read More</Link>
          </div>
        )
      })
    }
  }

  return (
    <div className="col-sm-9">
      <div className="blog-post-area">
        <h2 className="title text-center">Latest From our Blog</h2>
        {fetchData()}
        <div className="pagination-area">
          <ul className="pagination">
            <li><a className="active">1</a></li>
            <li><a>2</a></li>
            <li><a>3</a></li>
            <li><a><i className="fa fa-angle-double-right" /></a></li>
          </ul>
        </div>
      </div>
    </div>
  );
}
export default Blog;