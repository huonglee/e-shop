import React from "react";

function ListComment(props) {
  let { listcmt, getIdCha } = props;
  console.log("listcmt1:", listcmt);

  function renderListComment() {
    if (listcmt.length > 0) {
      return listcmt.map((value, i) => {
        if (value.id_comment == 0) {
          return (
            <React.Fragment key={i}>
              <li className="media" key={value.id}>
                <a className="pull-left">
                  <img className="media-object" src={"http://localhost:8080/laravel8/public/upload/user/avatar/" + value['image_user']} alt="" />
                </a>
                <div className="media-body">
                  <ul className="sinlge-post-meta">
                    <li><i className="fa fa-user" />{value["name_user"]}</li>
                    <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                    <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                  </ul>
                  <p>{value["comment"]}</p>
                  <a className="btn btn-primary" href="#cmt" id={value["id"]} onClick={handleReply}><i className="fa fa-reply" />Replay</a>
                </div>
              </li>
              {listcmt.map((value2, j) => {
                if (value.id == value2.id_comment) {
                  return (

                    <li className="media second-media" key={j}>
                      <a className="pull-left">
                        <img className="media-object" src={"http://localhost:8080/laravel8/public/upload/user/avatar/" + value2['image_user']} alt="" />
                      </a>
                      <div className="media-body">
                        <ul className="sinlge-post-meta">
                          <li><i className="fa fa-user" />{value2["name_user"]}</li>
                          <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                          <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                        </ul>
                        <p>{value2["comment"]}</p>
                        <a className="btn btn-primary" href="#cmt" id={value2["id"]} onClick={handleReply}><i className="fa fa-reply" />Replay</a>
                      </div>
                    </li>

                  )
                }
              })}
            </React.Fragment>
          )
        }
      })
    }
  }

  function handleReply(e) {
    getIdCha(e.target.id);
  }

  return (
    <div className="response-area">
      <h2>3 RESPONSES</h2>
      <ul className="media-list">
        {renderListComment()}
      </ul>
    </div>)
} export default ListComment;