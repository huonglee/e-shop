import axios from "axios";
import { useEffect, useState, useContext } from "react";

import { UserContext } from "../../UserContext";

function ProductWishlist() {
  const value = useContext(UserContext);
  const [products, setProducts] = useState([]);
  useEffect(() => {
    axios.get("http://localhost:8080/laravel8/public/api/product/wishlist")
      .then(
        response => {
          console.log("Res", response.data.data);
          setProducts(response.data.data);
        }
      )
      .catch(
        error => {
          console.log(error);
        }
      )
  }, []);

  // get id user:
  let auth;
  if (localStorage.getItem("userData")) {
    auth = (JSON.parse(localStorage.getItem("userData"))).Auth;
  }

  // get wishlist in localstorage:
  let productWishlist = [];
  if (localStorage.getItem("wishlistArray")) {
    productWishlist = JSON.parse(localStorage.getItem("wishlistArray"));
  }

  function renderWishlist() {
    let newData = [...products]
    let wishList = []
    // filter array wishlist:
    products.map((item, index) => {
      if (productWishlist.includes(item.id) == true) {
        let newArray = newData.filter(val => val == item);
        wishList = wishList.concat(newArray);
      }
    })
    console.log("wishlist array:", wishList);
    // map show ra table:
    if (wishList.length > 0) {
      return wishList.map((value, index) => {
        return (<tr key={index}>
          <td className="product_id">
            <p>{value.id}</p>
          </td>
          <td className="product_name">
            <p>{value.name}</p>
          </td>
          <td className="product_image">
            <a><img src={"http://localhost:8080/laravel8/public/upload/product/" + auth.id + "/" + JSON.parse(value['image'])[0]} alt="" /></a>
          </td>
          <td className="product_price">
            <p>${value.price}</p>
          </td>
          <td className="product_add_to_cart_action">
            <button>Add to cart</button>
          </td>
          <td className="product_delete_action">
            <button id={value.id} onClick={handleDelete}><i className="fa fa-times" /></button>
          </td>
        </tr>
        )
      })
    }
  }

  function handleDelete(e) {
    let idPro = e.target.id;
    console.log("id", idPro);
    let newData = [...products]
    newData.map((item, index) => {
      if (idPro == item["id"]) {
        newData = newData.filter(val => val != item);
        console.log("new Data", newData)
      }
    })
    setProducts(newData);
    console.log("newListProduct", newData);
    // xóa trong localstorage:
    let newlist = productWishlist.filter(val => val != idPro);
    // lưu vào localstorage:
    localStorage.setItem("wishlistArray", JSON.stringify(newlist));

    value.wishlistContext(CalProductWishlist());
  }

  // tinh tong sl product trong wishlist
  function CalProductWishlist() {
    let count = 0
    if (localStorage.getItem("wishlistArray")) {
      let wishlistArray = JSON.parse(localStorage.getItem("wishlistArray"))
      if (wishlistArray.length > 0) {
        count = wishlistArray.length;
      } else {
        count = "";
      }
    }
    // lưu vào trong localstorage:
    localStorage.setItem("WishlistQty", JSON.stringify(count));
    return count
  }

  return (
    <div className="col-sm-9 padding-right">
      <section id="cart_items">
        <div className="container">
          <div className="breadcrumbs">
            <ol className="breadcrumb">
              <li><a>Home</a></li>
              <li className="active">Product Wishlist</li>
            </ol>
          </div>
          <div className="table-responsive cart_info">
            <table className="table table-condensed">
              <thead>
                <tr className="cart_menu">
                  <td className="product_id">Id</td>
                  <td className="product_name">Name</td>
                  <td className="product_image">Image</td>
                  <td className="product_price">Price</td>
                  <td className="action">Action</td>
                  <td />
                </tr>
              </thead>
              <tbody>
                {renderWishlist()}
              </tbody>
            </table>
          </div>
        </div>
      </section>
    </div>
  )
}
export default ProductWishlist;