import { Button, Modal } from "react-bootstrap";
function ModalPopup(props) {
    let { linkImage, closeModal } = props
    return (
        <div className="static-modal">
            <Modal.Dialog>
                <Modal.Header>
                    <Modal.Title>Product Image</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <img src={linkImage} />
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={() => closeModal(false)}>Close</Button>
                    {/* <Button bsStyle="primary">Save changes</Button> */}
                </Modal.Footer>
            </Modal.Dialog>
        </div>
    )
}
export default ModalPopup;